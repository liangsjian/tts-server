#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import tornado.escape
import tornado.ioloop
import tornado.web
import os
import sys
import errno
import tempfile
import base64

from tornado.concurrent import Future
from tornado import gen, process
from tornado.options import define, options, parse_command_line

__VERSION__ = "v0.1.1"
BASE_DIR = os.path.dirname(__file__)

# 默认支持的人声, 如果配置选项voicelist指定，忽略
DEFAULT_VOICE = [ "voice_cmu_us_ahw_cg",
                 "voice_cmu_us_axb_cg",
                 "voice_cmu_us_fem_cg",
                 "voice_cmu_us_ksp_cg",
                 "voice_cmu_us_slt_cg",
                 "voice_cmu_us_aup_cg",
                 "voice_cmu_us_bdl_cg",
                 "voice_cmu_us_gka_cg",
                 "voice_cmu_us_rms_cg",
                 "voice_kal_diphone",
                 "voice_cmu_us_awb_cg",
                 "voice_cmu_us_clb_cg",
                 "voice_cmu_us_jmk_cg",
                 "voice_cmu_us_rxr_cg",
                 "voice_rab_diphone",
                 ]

ALL_VOICE = { item:item for item in DEFAULT_VOICE }

# 运行端口
define("port", default=8080, help="run on the given port", type=int)
# 调试模式
define("debug", default=False, help="run in debug mode")
# 默认人声，http请求未设置人声使用此默认人声类型
define("voice", help="set default voice")
# 设置此项会将临时文件进行缓存
define("cache", default=False, help="cache the temp file")
# 缓存文件夹,在设置cache的情况请确保正确设置此选项
define("cache_dir", default="./tempvoice", help="voice cache output directory")
# festival 安装的完整路径
define("festival", help="festival program install dir")
# define("text2wave", default="text2wave", help="text2wave program path")
# 配置文件路径，类python写法，请参考程序目录下的tts.conf
define("config", default="tts.conf", help="tts-server config file")
# 支持人声列表,为设置的话使用 DEFAULT_VOICE 中的选项
define("voicelist", help="server voice type list")
# gzip压缩
define("gzip", default=True, help="response gzip compress")
# 定时删除缓存文件夹时间，默认30分钟
define("cacheflush", default=60*30,
       help="time interval to flush cache directory",
       type=int)
# 系统的语音格式转换
define("fmtlist", default="wav,mp3", help="voice format")


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html", voicelist=ALL_VOICE.keys())
        # self.write('tts-server base on tornado and festival\n')

class TTSVoiceHandler(tornado.web.RequestHandler):
    """
    tts voice list
    """
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.write({"voicelist":list(ALL_VOICE.keys())})


class VoiceStaticHandler(tornado.web.StaticFileHandler):
    """ static file handler for voice"""
    def write_error(self, status_code, **kwargs):
        logging.debug('%d get voice file')
        if status_code == 404:
            self.set_header('Content-Type', 'application/json')
            self.write({
                "success": False,
                "error":{
                    "code":status_code,
                    "name":"can not found %s"% kwargs.getdefault("path",
                                                                 "voicefile")
                }
            })
        else:
            super(VoiceStaticHandler, self).write_error(status_code, **kwargs)

class TTSHandler(tornado.web.RequestHandler):
    """text to speech handler"""
    @gen.coroutine
    def _get_or_post(self):
        # 未设置语音情况使用默认语音
        voice = self.get_argument('voice',
                                  options.voice or ALL_VOICE.keys()[0])
        # 翻译的文本
        text = self.get_argument('text', None)
        # 语音格式，支持mp3与wav
        fmt = self.get_argument('fmt', 'wav')
        # 默认返回语音数据
        type = self.get_argument('type', 'content')
        self.set_header('Content-Type', 'application/json')

        if not text:
            self.set_status(400)
            status = {
                "success":False,
                "error":{"code":400, "name":"text is empty"}
            }
            self.write(status)
            return
        elif voice not in ALL_VOICE:
            self.set_status(400)
            status = {
                "success":False,
                "error":{"code":400, "name":"can not found voice %s"%voice}
            }
            self.write(status)
            return
        elif fmt not in options.fmtlist:
            self.set_status(400)
            status = {
                "success":False,
                "error":{
                    "code":400,
                    "name":"can not convert voice to %s format"%fmt
                }
            }
            self.write(status)
            return
        elif type not in ("content", "filename"):
            self.set_status(400)
            status = {
                "success":False,
                "error":{
                    "code":400,
                    "name":"form subimt type %s error"%type
                }
            }
            self.write(status)
            return


        # 临时文本文件
        textname = self._text_file(text)
        # 语音文件
        wavename = self._temp_name(options.cache_dir, '.wav')

        try:
            # 调用 text2wave 生成语音文件
            cmd = self._cmdline_text2wave(ALL_VOICE[voice], textname, wavename)
            # 非阻塞运行，需要使用shell=True，使用 token
            # args形式会有其他问题
            yield self.new_process(cmd, shell=True)
            logging.debug('wave2text generator %s', wavename)
            # festival 据了解只能输出wav格式，如果请求其他格式
            # 使用 lame进行转换
            if fmt != 'wav':
                tempwave = self._temp_name(options.cache_dir, '.'+fmt)
                self._cmdline_lame(wavename, tempwave)
                yield self.new_process(cmd, shell=True)
                wavename = tempwave
            if type == 'content':
                self._content_response(wavename, voice)
            elif type == 'filename':
                self._filename_response(wavename, voice)
        except Exception as e:
            raise e
        finally:
            try:
                if not options.debug and not options.cache:
                    os.unlink(wavename)
                os.unlink(textname)
            except Exception as e:
                pass
    @gen.coroutine
    def post(self):
        yield self._get_or_post()

    @gen.coroutine
    def get(self):
        yield self._get_or_post()

    def _content_response(self, wavename, voice):
        b64data = self._base64_wave(wavename)
        if not b64data:
            self.set_status(500)
            status = {
                "success": False,
                "error":{
                    "code":500,
                    "name":"can not encode %s to base64 string"%wavename
                }
            }
            logging.warning(status["error"]["name"])
            self.write(status)
            return
        status = {
            "success": True,
            "data":{
                "sound_base64":b64data,
                "voice": voice,
                "file": os.path.basename(wavename)
            }
        }
        self.write(status)

    def _filename_response(self, wavename, voice):
        # cache 选项开启的话，可以这样使用
        # 服务端返回语音文件的文件名，客户端根据文件名请求
        # 语音文件保存到 cahce_dir 当中， nginx 设置static
        # 路径指向 cache_dir 以供客户端获取语音文件
        if not options.cache:
            self.set_status(400)
            status = {
                "success": False,
                "error":{
                    "code":400,
                    "name":"tts-server close cache options"
                }
            }
            self.write(status)
        else:
            # 返回文件名，客户端可以获取 /tts/voice/<语音文件名>
            status = {
                "success": True,
                "data":{
                    "file": os.path.basename(wavename),
                    "voice": voice
                }
            }
            self.write(status)

    def _text_file(self, text):
        # 生成临时文本文件提供给text2wave调用
        try:
            f, textname = tempfile.mkstemp(dir=options.cache_dir)
            os.write(f, text)
            os.close(f)
            return textname
        except Exception as e:
            logging.warning(e)
            raise e

    def new_process(self, *args, **kwargs):
        """
        创建进程执行任务，非阻塞
        """
        p = process.Subprocess(*args, **kwargs)
        f = Future()
        p.set_exit_callback(lambda r: f.set_result(r))
        return f

    def _cmdline_text2wave(self, voice, textname, wavename):
        args = [os.path.join(options.festival or "", "text2wave"),
                '-eval', "'(%s)'"%voice,
                '-o', wavename,
                textname]
        return " ".join(args)

    def _cmdline_lame(self, src, dst):
        return "lame %s %s"%(src, dst)

    def _temp_name(self, dir, suffix):
        f, name = tempfile.mkstemp(dir=dir, suffix=suffix)
        os.close(f)
        return name

    def _base64_wave(self, filename):
        try:
            with open(filename, 'rb') as f:
                return base64.b64encode(f.read())
        except IOError as e:
            if e.errno == errno.ENOENT:
                logging.warning('can not open %s', filename)
        except Exception as e:
            logging.warning(e)

# 定时删除缓存文件夹
def flush_cache_dir():
    if options.cache_dir:
        logging.info("clear cache directory %s", options.cache_dir)
        if os.path.exists(options.cache_dir):
            for path, subdirs, files in os.walk(options.cache_dir):
                for name in files:
                    os.unlink(os.path.join(path, name))
        else:
            mkdir_p(options.cache_dir)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def option_handle():
    """处理命令与配置文件"""
    parse_command_line()
    if options.config:
        if os.path.exists(options.config):
            options.parse_config_file(options.config)
        else:
            print "can not find %s"%options.config
            print "usage:python %s --help"%os.path.basename(__file__)
            sys.exit(1)

    if options.voicelist:
        def _parse_voicelist():
            return tornado.escape.json_decode(options.voicelist)
        global ALL_VOICE
        ALL_VOICE = _parse_voicelist()
        logging.debug("conf voicelist: %s", ALL_VOICE)
    if options.cache_dir:
        mkdir_p(options.cache_dir)

def main():
    option_handle()
    handlers = [
        (r"/", MainHandler),
        (r"/tts", TTSHandler),
        (r"/tts/voicelist", TTSVoiceHandler)
    ]
    if options.cache:
        handlers.append((r"/tts/voice/([^/]+)", VoiceStaticHandler,
                         {"path": options.cache_dir}))


    app = tornado.web.Application(
        handlers,
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=options.debug,
        gzip=True)

    try:
        logging.info("start tts-server listen on port:%d", options.port)
        app.listen(options.port)
        tornado.ioloop.PeriodicCallback(flush_cache_dir,
                                        options.cacheflush*1000).start()
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.current().stop()


if __name__ == "__main__":
    main()
# vim:set et sw=4 ts=4:
