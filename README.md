# 目录

* [安装 Festival](#安装-festival)
* [HTTP 服务 tts-server](#http-服务-tts-server)
	* [安装](#安装)
	* [整体功能](#整体功能)
	* [WEB 接口描述](#web-接口描述)
		* [/tts](#tts)
		* [/tts/voicelist](#ttsvoicelist)
	* [测试](#测试)
* [配置文件](#配置文件)
* [帮助说明](#帮助说明)


# 安装 Festival

编译过程发现虚拟机上的Ubuntu14.04缺少curses库，预先安装

    sudo apt-get install ncurses-dev


现提供脚本直接执行安装即可，后面解释脚本执行安装的过程与细节

进入sst-server目录执行脚本`fest_build.sh`，安装 fetival,脚本会调用
wget程序进行下载源码包，请确保安装已经安装wget

    $ chmod +x fest_build.sh
    $ ./fest_build.sh

执行结束在tts-server/下可以得到 packed 和 build 两个目录 *festival* 可执行程序在`tts-server/build/festival/bin`

设置环境变量，在`~/.profile`文件下添加:

    PATH="$PATH:<目录>/tts-server/build/festival/bin"
    export PATH

测试是否安装正常，执行下面命令确认正常发音

    echo "hello world" | festival --tts

如果提示错误不能发音

    Linux: can't open /dev/dsp

修改文件`<目录>/tts-server/build/festival/lib/siteinit.scm `，添加下面三行lisp代码，主要让 festival 将语音文件保存后，调用系统程序 aplay 播放语音文件 (aplay在ubuntu中应该是默认安装)

```lisp
(Parameter.set 'Audio_Command "aplay $FILE")
(Parameter.set 'Audio_Method 'Audio_Command)
(Parameter.set 'Audio_Required_Format 'snd)
```

安装过程主要参考下面几篇文章：

[http://my.oschina.net/u/154340/blog/23968](http://my.oschina.net/u/154340/blog/23968)

[https://linuxtoy.org/archives/festival_on_ubuntu.html](https://linuxtoy.org/archives/festival_on_ubuntu.html)

[http://festvox.org/fest_build](http://festvox.org/fest_build)

[https://github.com/usc-isi-i2/festival-text-to-speech-service](https://github.com/usc-isi-i2/festival-text-to-speech-service)



# HTTP 服务 tts-server

## 安装
Http 服务器基于 python Tornado 编写, python版本为2.7

如果未安装 pip 请执行：

    $apt-get install pip

安装 Tornado

    $ pip install tornado==4.3

进入工程目录，运行服务器

    $ cd tts-server/
    $ python server.py --config=tts.conf

浏览器打开 `localhost:8080/` 即可执行测试

![tts-server demo](http://upload-images.jianshu.io/upload_images/1952606-5975328ac8ce7857.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


测试API接口获取json base64确认是否成功：

    curl "http://localhost:8080/tts?text=hello+world"

测试API接口获取二进制语音文件(web demo采用此方法)

    curl "http://localhost:8080/tts?text=hello&type=filename"
    # 得到下面输出
    {
        "data": {
            "voice": "voice_kal_diphone",
            "file": "tmppRouLG.wav"
        },
        "success": true
    }
    #通过返回的json的语音文件 "file" 获取
    wget "http://localhost:8080/tts/voice/tmppRouLG.wav"


debug模式可以在服务器目录下面的`tempvoice/`目录找到对应语音文件，具体配置可以通过修改tts.conf文件实现

## 整体功能
暂时命名该服务程序为 tts-server，代码主要过程为

1. 语音请求 ->  festival -> 返回 json 数据（包含base64语音文件）
2. 语音请求 ->  festival -> 返回语音文件url路径->客户通过语音文件url请求

第二种方法如果不希望使用Tornado返回二进制数据，可以在其前面
加个nginx作为静态文件服务器，其他请求转发给tornado

目前实现的功能：
- 文本转语音
- 查看服务器支持语音列表
- 定时清理缓存文件夹
- 支持命令配置与文件配置
- 支持存储语音到目录，通过nginx返回语音
- 支持多实例与nginx反向代理
- 支持日志功能，可以输入 `--help` 命令查看具体使用


## WEB 接口描述

### /tts

同时支持 GET 与 POST 方法，返回格式为 json

参数说明：

- text 转换文本
- voice 人声选项，具体参数下面有说明
- fmt 语音格式mp3/wav，默认为wav
- type json(默认)或者cache，具体使用参看下面说明

例子：

    curl "http://localhost:8080/tts?text=hello&type=json"

返回格式：

1. 成功

```
{
    "success":true,
    "data":{
        "sound_base64":"***",
        "voice": "语音的人声类型",
        "file":"本地文件名" # 后面有说明,用于扩展
    }
}
```

2. 失败

```
{
    "success":false,
    "error":{
        "code":HTTP状态码,
        "name":"文字描述"
    }
}
```

如果不希望将语音文件转为base64，可以通过下面方法获取二进制文件

例子：

    # 下面调用不会返回二进制语音数据，只返回文件名
    curl "http://localhost:8080/tts?text=hello&voice=voice_cmu_us_ahw_cg&type=cache"
    #通过返回的json的语音文件获取二进制
    wget "http://localhost:8080/tts/voice/<语音文件名>"


第一个调用返回的json格式为

    {
        "success":true,
        "data":{
            "file":"语音本地文件名",
            "voice": "语音的人声类型"
        }
    }


### /tts/voicelist

返回服务器支持的人声列表如：

    {"voicelist": [
        "voice_cmu_us_ahw_cg",
        "voice_cmu_us_axb_cg",
        "voice_cmu_us_fem_cg"
    ]}

查看系统支持的人声可以进入 festival 交互命令执行：

    festival> (voice.list)

注意：设置人声的时候需要加上前缀 voice_

安装其他人声可参考

[http://ubuntuforums.org/showthread.php?t=751169](http://ubuntuforums.org/showthread.php?t=751169)]

## 测试

在程序目录下有测试用例 test.py，基于 tornado.test 编写

测试用例可以输入与服务器一样的配置参数，默认读取目录下的 tts.conf
配置文件(命令行输入参数会与unittest冲突，暂不支持命令行输入参数)

    python test.py

执行测试用例后即可看到日志输出与过程，主要进行测试的功能有：

- 参数正确性
- 服务器返回值验证
- 验证服务器安装的每种人声正常使用
- 语音文件格式正确性

# 配置文件

目录下面提供了默认的配置文件模板 tts.conf 以及 相应参数说明，
可根据需要进行修改

# 帮助说明

    $ python server.py --help
    Usage: server.py [OPTIONS]

    Options:

      --help                           show this help information

    tornado/log.py options:

      --log-file-max-size              max size of log files before rollover
                                       (default 100000000)
      --log-file-num-backups           number of log files to keep (default 10)
      --log-file-prefix=PATH           Path prefix for log files. Note that if you
                                       are running multiple tornado processes,
                                       log_file_prefix must be different for each
                                       of them (e.g. include the port number)
      --log-rotate-interval            The interval value of timed rotating
                                       (default 1)
      --log-rotate-mode                The mode of rotating files(time or size)
                                       (default size)
      --log-rotate-when                specify the type of TimedRotatingFileHandler
                                       interval other options:('S', 'M', 'H', 'D',
                                       'W0'-'W6') (default midnight)
      --log-to-stderr                  Send log output to stderr (colorized if
                                       possible). By default use stderr if
                                       --log_file_prefix is not set and no other
                                       logging is configured.
      --logging=debug|info|warning|error|none
                                       Set the Python log level. If 'none', tornado
                                       won't touch the logging configuration.
                                       (default info)
    tts-server/server.py options:

    --cache                          cache the temp file (default False)
    --cache-dir                      voice cache output directory (default
                                    ./tempvoice)
    --cacheflush                     time interval to flush cache directory
                                    (default 1800)
    --config                         tts-server config file (default tts.conf)
    --debug                          run in debug mode (default False)
    --festival                       festival program install dir
    --fmtlist                        voice format (default wav,mp3)
    --gzip                           response gzip compress (default True)
    --port                           run on the given port (default 8080)
    --voice                          set default voice
    --voicelist                      server voice type list

不需要日志功能的话忽略 log.py 部分，主要解释一下 server.py 
部分的参数含义与作用，具体设置可参考 tts.conf

- --cache 缓存临时文件到 cache-dir 目录，提供静态文件获取功能，建议开启（需求外提供）
- --cache-dir 设置语音缓存目录，不设置的话使用系统默认目录，使用完毕会自动删除
- --cacheflush 定时清空缓存时间，单位秒（s）
- --config 配置文件路径，可以参考目录下 tts.conf，命令行中的配置会覆盖配置文件中的配置
- --debug 调试模式，此模式不会删除缓存文件
- --festival 编译安装好的 festival 目录路径，确保设置正确，建议使用完整路径，即使已经将此路径设置到环境变量
- --fmtlist 支持的语音文件格式，经测试mp3与wav格式正常，其他格式请自行确认
- --gzip 返回响应是否需要gzip压缩
- --port 监听端口
- --voice 默认使用的人声
- --voicelist 人声列表，安装有的人声类型，配置格式参考配置文件模板


