#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, with_statement

import base64
import os
import unittest
import tornado
from tornado import escape, httputil
from tornado.web import Application, RequestHandler, url
from tornado.testing import AsyncHTTPTestCase
from tornado.options import options, define
import server

# 测试所有人声是否正常
define("test_all_voice", default=False,
       help="test all voice in server config list")

class TTSTestCase(AsyncHTTPTestCase):
    def get_app(self):
        return Application([
            url("/tts", server.TTSHandler),
            url("/tts/voicelist", server.TTSVoiceHandler),
            url("/tts/voice/([^/]+)", server.VoiceStaticHandler,
                {"path":options.cache_dir}),
            ],)
            # log_function=lambda h: h)

    def test_header_json(self):
        response = self.fetch("/tts?text=yes")
        self.assertTrue('application/json' in
                        [ i.strip() for i in response.headers["Content-Type"].split(';')])

    def test_empty_text(self):
        response = self.fetch("/tts?text=")
        self.assertEqual(response.code, 400)


    def test_empty_voice(self):
        response = self.fetch("/tts?text=hello&voice=")
        self.assertEqual(response.code, 400)

    # 返回的是默认人声
    def test_default_voice(self):
        response = self.fetch("/tts?text=hello")
        status = escape.json_decode(response.body)
        self.assertEqual(response.code, 200)
        self.assertEqual(status["data"]["voice"], options.voice)

    def test_empty_param(self):
        response = self.fetch("/tts")
        status = escape.json_decode(response.body)
        self.assertEqual(response.code, 400)
        self.assertFalse(status["success"])
        self.assertEqual(status["error"]["code"], 400)
        self.assertEqual(status["error"]["name"], "text is empty")

    # 测试不存在的人声会返回错误
    def test_not_exist_voice(self):
        voice = "not_exist_voice"
        response = self.fetch("/tts?text=hello&voice=%s"%voice)
        status = escape.json_decode(response.body)
        self.assertEqual(response.code, 400)
        self.assertFalse(status["success"])
        self.assertEqual(status["error"]["code"], 400)
        self.assertEqual(status["error"]["name"], "can not found voice %s"%voice)

    # 支持人声列表接口
    def test_voice_list(self):
        response = self.fetch("/tts/voicelist")
        self.assertEqual(response.code, 200)
        self.assertTrue('application/json' in
                        [ i.strip() for i in response.headers["Content-Type"].split(';')])
        status = escape.json_decode(response.body)
        self.assertEqual(status["voicelist"], list(server.ALL_VOICE))

    def _test_voice(self, text, voice):
        response = self.fetch(httputil.url_concat("/tts",
                                                  {"text":text, "voice":voice}))
        self.assertEqual(response.code, 200)
        status = escape.json_decode(response.body)
        self.assertTrue(status["success"])
        self.assertGreater(len(status["data"]["sound_base64"]), 0)
        self.assertEqual(status["data"]["voice"], voice)

    # 测试所有人声正常可用, 配置文件tts.conf 开启test_all_voice选项
    def test_hello_all_voice(self):
        text = "hello world"
        if not options.test_all_voice:
            return
        for voice in server.ALL_VOICE:
            self._test_voice(text, voice)

    # 响应的语音base64数据是否与本地缓存语音文件数据一致
    def test_voice_base64_data(self):
        if not options.cache_dir: return
        response = self.fetch("/tts?text=hello")
        status = escape.json_decode(response.body)
        self.assertEqual(response.code, 200)
        self.assertTrue(status["success"])
        self.assertGreater(len(status["data"]["file"]), 0)
        # 未设置人声类型，返回的应该是默认类型
        self.assertEqual(status["data"]["voice"], options.voice)
        voicedata = base64.b64decode(status["data"]["sound_base64"])
        with open(os.path.join(options.cache_dir,
                               status["data"]["file"]), "rb") as f:
            filedata = f.read()
        self.assertEqual(filedata, voicedata)

    # 需要配置cache功能
    def test_voice_filename(self):
        response = self.fetch("/tts?text=hello&type=filename")
        status = escape.json_decode(response.body)
        if response.code == 400:
            self.assertEqual(status["error"]["name"],
                            "tts-server close cache options")
        elif response.code == 200:
            self.assertTrue(status["success"])
            self.assertGreater(len(status["data"]["file"]), 0)
            # 未设置人生类型，返回的应该是默认类型
            self.assertEqual(status["data"]["voice"], options.voice)
        else:
            self.assertTrue(0)

    # 确保通过web响应的语音与服务器本地语音数据一致性
    def test_voice_cache_data(self):
        if not options.cache_dir: return
        # 1.请求cache语音
        response = self.fetch("/tts?text=hello&type=filename")
        status = escape.json_decode(response.body)
        self.assertEqual(response.code, 200)
        # 2. 获取静态语音文件，验证一直
        response = self.fetch("/tts/voice/%s"%status["data"]["file"])
        self.assertEqual(response.code, 200)
        with open(os.path.join(options.cache_dir,
                               status["data"]["file"]), "rb") as f:
            filedata = f.read()
        self.assertEqual(filedata, response.body)

    def test_all_voice_fmt(self):
        for fmt in options.fmtlist:
            if len(fmt): continue
            response = self.fetch("/tts?text=hello&type=filename&fmt=%s"%fmt)
            status = escape.json_decode(response.body)
            self.assertEqual(response.code, 200)
            self.assertTrue(status["success"])
            # 返回的文件格式与请求格式一致
            self.assertEqual(os.path.splitext(status['data']['name'][1]), fmt)

    def test_not_exist_fmt(self):
        fmt = "no_exist_fmt"
        response = self.fetch("/tts?text=hello&type=filename&fmt=%s"%fmt)
        status = escape.json_decode(response.body)
        self.assertEqual(response.code, 400)
        self.assertFalse(status["success"])
        self.assertEqual(status["error"]["name"],
                         "can not convert voice to %s format"%fmt)

    def test_type_voice(self):
        type = "not_exist_type"

        response = self.fetch("/tts?text=hello&type=%s"%type)
        status = escape.json_decode(response.body)
        self.assertEqual(response.code, 400)
        self.assertFalse(status["success"])
        self.assertEqual(status["error"]["name"],
                         "form subimt type %s error"%type)



if __name__ == '__main__':
    server.option_handle()
    unittest.main()
# vim:set et sw=4 ts=4 tw=80:
