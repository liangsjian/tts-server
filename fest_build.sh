#!/bin/sh
# Download the code and voices, if not already downloaded
# 脚本主要参考 http://festvox.org/fest_build

# 语音格式转换程序，将wav转mp3
apt-get install lame


# 脚本中讲全部这些安装包下载到 packed 目录中
# 如果已经有该目录则不在下载
if [ ! -d packed ]
then

mkdir packed
cd packed
# 下载festival源码包
wget http://festvox.org/packed/festival/2.4/festival-2.4-release.tar.gz
# speech-tool是一个框架库，festival依赖改库
wget http://festvox.org/packed/festival/2.4/speech_tools-2.4-release.tar.gz

# 其他包festlex_*
wget http://festvox.org/packed/festival/2.4/festlex_CMU.tar.gz
wget http://festvox.org/packed/festival/2.4/festlex_OALD.tar.gz
wget http://festvox.org/packed/festival/2.4/festlex_POSLEX.tar.gz

# 下载声语音包cmu
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_ahw_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_aup_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_awb_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_axb_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_bdl_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_clb_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_fem_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_gka_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_jmk_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_ksp_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_rms_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_rxr_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_cmu_us_slt_cg.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_kallpc16k.tar.gz
wget http://festvox.org/packed/festival/2.4/voices/festvox_rablpc16k.tar.gz


#下载CMU Arctic音频
mkdir cmu_tmp
cd cmu_tmp/
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_awb_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_bdl_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_clb_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_jmk_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_rms_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_slt_arctic-0.95-release.tar.bz2
cd ..

fi

# 创建build目录，并将所有压缩包解压到build目录之下
# Unpack the code and voices
mkdir build
cd build

# 解压cmu语音
for i in ../packed/*.gz
do
   tar zxvf $i
done



#解压cmu arctic语音
if [ ! -d clunits ]; then
    mkdir clunits
    cd clunits
    for t in ../../packed/cmu_tmp/*.bz2 ; do tar xf $t ; done
    for d in `ls ./` ; do
    if [[ "$d" =~ "cmu_us_" ]] ; then
    mv -f "${d}" "../festival/lib/voices/us/${d}_clunits"
    fi ; done
    cd ..
    rm -rf clunits
fi



# Set up the environment variables for voice building
export ESTDIR=`pwd`/speech_tools

# 必须先编译 speech-tool
cd speech_tools
./configure
make
#make test
cd ..

# 编译 festival
cd festival
./configure
make
#make test


cd ..
#vim:set et sw=4 ts=4 tw=80:
